package org.sas.sudoku;

public class Sudoku {
	public static final int SIZE = 9;

	// sample input
	private int grid[][] = 
		  { { 3, 0, 6, 5, 0, 8, 4, 0, 0 },
			{ 5, 2, 0, 0, 0, 0, 0, 0, 0 }, 
			{ 0, 8, 7, 0, 0, 0, 0, 3, 1 }, 
			{ 0, 0, 3, 0, 1, 0, 0, 8, 0 }, 
			{ 9, 0, 0, 8, 6, 3, 0, 0, 5 }, 
			{ 0, 5, 0, 0, 9, 0, 6, 0, 0 }, 
			{ 1, 3, 0, 0, 0, 0, 2, 5, 0 }, 
			{ 0, 0, 0, 0, 0, 0, 0, 7, 4 }, 
			{ 0, 0, 5, 2, 0, 6, 3, 0, 0 } };
	
	public boolean solve(){
		return solve(new Cell(0,0));
	}

	private boolean solve(Cell cell) {
		if (null == cell)
			return true;
		
		if( 0 != grid[cell.getRow()][cell.getCol()])
			return solve(nextCell(cell));
		
		for (int value = 1; value <= SIZE; ++value){
			if(!isValid(cell,value))
				continue;
			
			grid[cell.getRow()][cell.getCol()] = value;
			if(solve(nextCell(cell)))
				return true;
			
			grid[cell.getRow()][cell.getCol()] = 0;
			
		}
		return false;
	}

	private boolean isValid(Cell cell, int value) {
		for (int i = 0; i < SIZE; ++i){
			if (checkInRow(cell, value, i) || checkInCol(cell, value, i))
				return false;
		}
		
		return checkInSubGrid(cell, value);
	}

	private boolean checkInSubGrid(Cell cell, int value) {
		int x0 = cell.getRow() / 3 * 3;
		int x1 = x0 + 3;
		int y0 = cell.getCol() /3 * 3;
		int y1 = y0 + 3;

		for ( int x = x0; x < x1; ++x)
			for (int y = y0; y < y1; ++y )
				if ( value == grid[x][y])
					return false;
		
		return true;
	}

	private boolean checkInCol(Cell cell, int value, int i) {
		return value == grid[i][cell.getCol()];
	}

	private boolean checkInRow(Cell cell, int value, int i) {
		return value == grid[cell.getRow()][i];
	}

	private Cell nextCell(Cell cell) {
		int row = cell.getRow();
		int col = cell.getCol();
		
		if(++col >= SIZE){
			col = 0;
			++row;
		}
		if(row >= SIZE){
			return null;
		}
		return new Cell(row, col);
	}
	
	public void printGrid(){
		for(int[] row : grid){
			for(int value : row)
				System.out.print(value);
			System.out.println();
		}
	}
	
	public static void main(String... args){
		Sudoku sudoku = new Sudoku();
		if(!sudoku.solve())
			System.out.println("Cannot solve!");
		else{
			System.out.println("Solution: ");
			sudoku.printGrid();
		}
	}
}
