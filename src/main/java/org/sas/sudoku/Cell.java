package org.sas.sudoku;

public class Cell {
	private final int row, col;

	public Cell(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}
}
